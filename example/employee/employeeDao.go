package employee
//https://flaviocopes.com/golang-sql-database/
import (
	"database/sql"
	"fmt"
	_ "fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	_ "github.com/jmoiron/sqlx"
	"log"
)

type DataBaseDriver struct {
	userName string
	password string
	database string
	port     int
	db       *sql.DB
}

func (d *DataBaseDriver)connectToDb() (err error) {
	db, err := sql.Open("mysql", d.userName + ":"+ d.password  + "@/"+ d.database)
	if err != nil {
		log.Fatal("error happening")
		return err
	}
	d.db = db;
	return err;
}

func(d *DataBaseDriver) executeStatement() {

	//// Prepare statement for inserting data
	stmtIns, err := d.db.Prepare("INSERT INTO squareNum VALUES( ?, ? )") // ? = placeholder
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer stmtIns.Close() // Close the statement when we leave main() / the program terminates
	//
	//// Prepare statement for reading data
	//stmtOut, err := db.Prepare("SELECT squareNumber FROM squarenum WHERE number = ?")
	//if err != nil {
	//	panic(err.Error()) // proper error handling instead of panic in your app
	//}
	//defer stmtOut.Close()
	//
	//// Insert square numbers for 0-24 in the database
	//for i := 0; i < 25; i++ {
	//	_, err = stmtIns.Exec(i, (i * i)) // Insert tuples (i, i^2)
	//	if err != nil {
	//		panic(err.Error()) // proper error handling instead of panic in your app
	//	}
	//}
	//
	//var squareNum int // we "scan" the result in here
	//
	//// Query the square-number of 13
	//err = stmtOut.QueryRow(13).Scan(&squareNum) // WHERE number = 13
	//if err != nil {
	//	panic(err.Error()) // proper error handling instead of panic in your app
	//}
	//fmt.Printf("The square number of 13 is: %d", squareNum)
	//
	//// Query another number.. 1 maybe?
	//err = stmtOut.QueryRow(1).Scan(&squareNum) // WHERE number = 1
	//if err != nil {
	//	panic(err.Error()) // proper error handling instead of panic in your app
	//}
	//fmt.Printf("The square number of 1 is: %d", squareNum)
}

func readData() { // Execute the query
	//rows, err := db.Query("SELECT * FROM table")
	//if err != nil {
	//	panic(err.Error()) // proper error handling instead of panic in your app
	//}
	//
	//// Get column names
	//columns, err := rows.Columns()
	//if err != nil {
	//	panic(err.Error()) // proper error handling instead of panic in your app
	//}
	//
	//// Make a slice for the values
	//values := make([]sql.RawBytes, len(columns))
	//
	//// rows.Scan wants '[]interface{}' as an argument, so we must copy the
	//// references into such a slice
	//// See http://code.google.com/p/go-wiki/wiki/InterfaceSlice for details
	//scanArgs := make([]interface{}, len(values))
	//for i := range values {
	//	scanArgs[i] = &values[i]
	//}
	//
	//// Fetch rows
	//for rows.Next() {
	//	// get RawBytes from data
	//	err = rows.Scan(scanArgs...)
	//	if err != nil {
	//		panic(err.Error()) // proper error handling instead of panic in your app
	//	}
	//
	//	// Now do something with the data.
	//	// Here we just print each column as a string.
	//	var value string
	//	for i, col := range values {
	//		// Here we can check if the value is nil (NULL value)
	//		if col == nil {
	//			value = "NULL"
	//		} else {
	//			value = string(col)
	//		}
	//		fmt.Println(columns[i], ": ", value)
	//	}
	//	fmt.Println("-----------------------------------")
	//}
	//if err = rows.Err(); err != nil {
	//	panic(err.Error()) // proper error handling instead of panic in your app
	//}
}


var schema = `
CREATE TABLE person (
    first_name text,
    last_name text,
    email text
);

CREATE TABLE place (
    country text,
    city text NULL,
    telcode integer
)`

type Person struct {
	FirstName string `db:"first_name"`
	LastName  string `db:"last_name"`
	Email     string
}

type Place struct {
	Country string
	City    sql.NullString
	TelCode int
}

func mainExample() {
	// this Pings the database trying to connect, panics on error
	// use sqlx.Open() for sql.Open() semantics
	db, err := sqlx.Connect("postgres", "user=foo dbname=bar sslmode=disable")
	if err != nil {
		log.Fatalln(err)
	}

	// exec the schema or fail; multi-statement Exec behavior varies between
	// database drivers;  pq will exec them all, sqlite3 won't, ymmv
	db.MustExec(schema)

	tx := db.MustBegin()
	tx.MustExec("INSERT INTO person (first_name, last_name, email) VALUES ($1, $2, $3)", "Jason", "Moiron", "jmoiron@jmoiron.net")
	tx.MustExec("INSERT INTO person (first_name, last_name, email) VALUES ($1, $2, $3)", "John", "Doe", "johndoeDNE@gmail.net")
	tx.MustExec("INSERT INTO place (country, city, telcode) VALUES ($1, $2, $3)", "United States", "New York", "1")
	tx.MustExec("INSERT INTO place (country, telcode) VALUES ($1, $2)", "Hong Kong", "852")
	tx.MustExec("INSERT INTO place (country, telcode) VALUES ($1, $2)", "Singapore", "65")
	// Named queries can use structs, so if you have an existing struct (i.e. person := &Person{}) that you have populated, you can pass it in as &person
	tx.NamedExec("INSERT INTO person (first_name, last_name, email) VALUES (:first_name, :last_name, :email)", &Person{"Jane", "Citizen", "jane.citzen@example.com"})
	tx.Commit()

	// Query the database, storing results in a []Person (wrapped in []interface{})
	people := []Person{}
	db.Select(&people, "SELECT * FROM person ORDER BY first_name ASC")
	jason, john := people[0], people[1]

	fmt.Printf("%#v\n%#v", jason, john)
	// Person{FirstName:"Jason", LastName:"Moiron", Email:"jmoiron@jmoiron.net"}
	// Person{FirstName:"John", LastName:"Doe", Email:"johndoeDNE@gmail.net"}

	// You can also get a single result, a la QueryRow
	jason = Person{}
	err = db.Get(&jason, "SELECT * FROM person WHERE first_name=$1", "Jason")
	fmt.Printf("%#v\n", jason)
	// Person{FirstName:"Jason", LastName:"Moiron", Email:"jmoiron@jmoiron.net"}

	// if you have null fields and use SELECT *, you must use sql.Null* in your struct
	places := []Place{}
	err = db.Select(&places, "SELECT * FROM place ORDER BY telcode ASC")
	if err != nil {
		fmt.Println(err)
		return
	}
	usa, singsing, honkers := places[0], places[1], places[2]

	fmt.Printf("%#v\n%#v\n%#v\n", usa, singsing, honkers)
	// Place{Country:"United States", City:sql.NullString{String:"New York", Valid:true}, TelCode:1}
	// Place{Country:"Singapore", City:sql.NullString{String:"", Valid:false}, TelCode:65}
	// Place{Country:"Hong Kong", City:sql.NullString{String:"", Valid:false}, TelCode:852}

	// Loop through rows using only one struct
	place := Place{}
	rows, err := db.Queryx("SELECT * FROM place")
	for rows.Next() {
		err := rows.StructScan(&place)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Printf("%#v\n", place)
	}
	// Place{Country:"United States", City:sql.NullString{String:"New York", Valid:true}, TelCode:1}
	// Place{Country:"Hong Kong", City:sql.NullString{String:"", Valid:false}, TelCode:852}
	// Place{Country:"Singapore", City:sql.NullString{String:"", Valid:false}, TelCode:65}

	// Named queries, using `:name` as the bindvar.  Automatic bindvar support
	// which takes into account the dbtype based on the driverName on sqlx.Open/Connect
	_, err = db.NamedExec(`INSERT INTO person (first_name,last_name,email) VALUES (:first,:last,:email)`,
		map[string]interface{}{
			"first": "Bin",
			"last":  "Smuth",
			"email": "bensmith@allblacks.nz",
		})

	// Selects Mr. Smith from the database
	rows, err = db.NamedQuery(`SELECT * FROM person WHERE first_name=:fn`, map[string]interface{}{"fn": "Bin"})

	// Named queries can also use structs.  Their bind names follow the same rules
	// as the name -> db mapping, so struct fields are lowercased and the `db` tag
	// is taken into consideration.
	rows, err = db.NamedQuery(`SELECT * FROM person WHERE first_name=:first_name`, jason)
	//type Place struct {
	//	Country       string
	//	City          sql.NullString
	//	TelephoneCode int `db:"telcode"`
	//}
	//
	//rows, err := db.Queryx("SELECT * FROM place")
	//for rows.Next() {
	//	var p Place
	//	err = rows.StructScan(&p)
	//}
}