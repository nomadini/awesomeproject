package request_processing

import (
	"awesomeProject/DataProcessorService/kafka_consumer"
	"github.com/spf13/viper"
)

//use this https://github.com/lovoo/goka  streaming library for kafka


type KafkaConsumerModule struct {
	totalCount int;
	countInLastMinute int;
	consumer *kafka_consumer.KafkaConsumer
}

func (m KafkaConsumerModule) Process(c *ReqContext) {
	m.totalCount++;
	m.countInLastMinute++;
}

func (m KafkaConsumerModule) startConsumerThread() {

}

func NewKafkaConsumerModule() *KafkaConsumerModule {
	module := KafkaConsumerModule{}
	consumer := kafka_consumer.New(viper.GetString("kafka.server"),
		viper.GetString("kafka.groupId"),
		viper.GetString("kafka.offset"))
	consumer.ConsumeTopic(viper.GetString("impressionTopicName"))
	module.consumer = consumer
	return &module
}
