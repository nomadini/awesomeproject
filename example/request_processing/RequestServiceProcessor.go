package request_processing

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/go-kit/kit/endpoint"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
	"strings"
)
// ErrEmpty is returned when an input string is empty.
var ErrEmpty = errors.New("empty string")

// RequestProcessor provides operations on strings.
type RequestProcessorInt interface {
	Uppercase(string) (string, error)
	Count(string) int
	Process(ReqContext)
	initPipeLine()
}

// RequestProcessor is a concrete implementation of RequestProcessor
type RequestProcessor struct {
	pipeLine []Module
	consumerModule *KafkaConsumerModule
	MessageChannel chan string
}

type pipeLineRequest struct {
	name string `json:"name"`
	id int64 `json:"id"`
}

type pipeLineResponse struct {
	name string `json:"name"`
	id int64 `json:"id"`
}

type ReqContext struct {
	OriginalRequest pipeLineRequest
	finalResponse   pipeLineResponse
	err             error
	done chan struct{}
}

type Module interface {
	Process(c *ReqContext)
}

func (r RequestProcessor) InitPipeLine() {
	//var counter RequestCounterModule
	counterModule := RequestCounterModule{}

	r.consumerModule = NewKafkaConsumerModule()
	kafkaProducerModule := NewRequestProducer(viper.GetString("kafka.server"))
	r.MessageChannel = r.consumerModule.consumer.MessageChannel
	done := make(chan struct{})
	go kafkaProducerModule.Produce(done, viper.GetString("impressionTopicName"))
	go r.ProcessFromKafka(done)

	var pipeLine = []Module {counterModule}
	r.pipeLine = pipeLine;
}

func (RequestProcessor) Uppercase(s string) (string, error) {
	if s == "" {
		return "", ErrEmpty
	}
	return strings.ToUpper(s), nil
}

func (RequestProcessor) Count(s string) int {
	return len(s)
}

func (r RequestProcessor) Process(c ReqContext) {
	log.WithFields(log.Fields{"msg": c.OriginalRequest}).Info("processing a message")

	for _, mod := range r.pipeLine {
		mod.Process(&c)
	}
}


func (r RequestProcessor) ProcessFromKafka(done chan struct{}) {
	for {
		select {
		case newMessage := <- r.MessageChannel:
				r.process(newMessage)
		case <-done:
			return
		}
	}
}

func (r RequestProcessor) process(msg string) {
	c := ReqContext{}
	r.Process(c)
}

func MakePipeLineEndpoint(svc RequestProcessor) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(pipeLineRequest)
		var context ReqContext
		context.OriginalRequest = req

		svc.Process(context)
		if context.err != nil {
			return context.finalResponse, nil
		}
		return context.err, nil
	}
}

func DecodePipeLineRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request pipeLineRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}
