package request_processing

type AwsPersistenceModule struct {
	totalCount int;
	countInLastMinute int;
}

func (m AwsPersistenceModule) Process(c *ReqContext) {
	m.totalCount++;
	m.countInLastMinute++;
}

func NewAwsPersistenceModule() *AwsPersistenceModule {
	return &AwsPersistenceModule{}
}

var awsPersistenceModule = NewAwsPersistenceModule()
