package request_processing

import (
	log "github.com/sirupsen/logrus"
	"sync"
	"time"
)

type RequestCounterModule struct {
	totalCount int;
	countInLastMinute int;
}

func (m RequestCounterModule) Process(c *ReqContext) {
	m.totalCount++;
	m.countInLastMinute++;
}

type Counter struct {
	mu sync.Mutex
	value int
}
func(c *Counter) increment() {
	c.mu.Lock()
	c.value++;
	defer c.mu.Unlock()
}

func (c *Counter) CountEveryMinute(ctx *ReqContext) {
	ticker := time.NewTicker(1 * time.Minute)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			log.WithFields(log.Fields{
				"animal": "walrus",
			}).Info("A walrus appears")
		case <-ctx.done:
			return
		}
	}
}
