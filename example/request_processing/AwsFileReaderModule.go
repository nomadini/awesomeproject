package request_processing

type AwsFileReaderModule struct {
	totalCount int;
	countInLastMinute int;
}

func (m AwsFileReaderModule) Process(c *ReqContext) {
	m.totalCount++;
	m.countInLastMinute++;
}

func NewAwsFileReaderModule() *AwsFileReaderModule {
	return &AwsFileReaderModule{}
}


var awsFileReaderModule = NewAwsFileReaderModule()