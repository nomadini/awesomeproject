package request_processing

import (
	"awesomeProject/DataProcessorService/kafka_consumer"
	"awesomeProject/example/util"
	"github.com/go-kit/kit/metrics"
	"github.com/go-kit/kit/metrics/prometheus"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"time"
)

type RequestProducer struct {
	produer *kafka_consumer.KafkaProducer;

}

func NewRequestProducer(brokers string) *RequestProducer {
	s := &RequestProducer{}
	s.produer = kafka_consumer.NewKafkaProducer(brokers)
	return s
}

func (m *RequestProducer) Produce(done chan struct{}, topic string) {

	var duration metrics.Histogram
	{
		// Endpoint-level metrics.
		duration = prometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "example",
			Subsystem: "addsvc",
			Name:      "request_duration_seconds",
			Help:      "Request duration in seconds.",
		}, []string{"method", "success"})
	}

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			msg := util.RandomString(6)
			log.WithFields(log.Fields{
				"msg": msg,
			}).Debug("publishing message")
			duration.With("method", "messageSent", "success", "true").Observe(1)
			m.produer.SendMessage(msg, msg, topic);
			case <-done:
				return
		}
	}

}
