package main

import (
	"awesomeProject/DataProcessorService/kafka_consumer"
	"awesomeProject/example/employee"
	"awesomeProject/example/request_processing"
	_ "awesomeProject/example/request_processing"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-kit/kit/endpoint"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
	"os"

	httptransport "github.com/go-kit/kit/transport/http"
)


// Transports expose the service to the network. In this first example we utilize JSON over HTTP.
func main() {
	initialize()
	e1 := employee.New("Sam", "Adolf", 30, 20)
	e1.LeavesRemaining()

	svc := request_processing.RequestProcessor{}
	svc.InitPipeLine()

	uppercaseHandler := httptransport.NewServer(
		MakeUppercaseEndpoint(svc),
		decodeUppercaseRequest,
		encodeResponse,
	)

	pipeLineHandler := httptransport.NewServer(
		request_processing.MakePipeLineEndpoint(svc),
		request_processing.DecodePipeLineRequest,
		encodeResponse,
	)

	countHandler := httptransport.NewServer(
		makeCountEndpoint(svc),
		decodeCountRequest,
		encodeResponse,
	)

	http.Handle("/uppercase", uppercaseHandler)
	http.Handle("/count", countHandler)
	http.Handle("/process", pipeLineHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))

}

func initialize() {
	readConfig()
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})
	var consumer = kafka_consumer.KafkaConsumer{};
	consumer.ConnectToKafka(viper.GetString("kafka.server"),
		viper.GetString("kafka.groupId"),
		viper.GetString("kafka.offset"));

	viper.Get("name");

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.InfoLevel)
}

func readConfig() {
	viper.SetConfigName("config.yaml") // name of config file (without extension)
	viper.AddConfigPath("./config/")   // path to look for the config file in
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}

func makeCountEndpoint(svc request_processing.RequestProcessor) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(countRequest)
		v := svc.Count(req.S)
		return countResponse{v}, nil
	}
}

func decodeUppercaseRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request uppercaseRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeCountRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request countRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

// Endpoints are a primary abstraction in go-kit. An endpoint represents a single RPC (method in our service interface)
func MakeUppercaseEndpoint(svc request_processing.RequestProcessor) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(uppercaseRequest)
		v, err := svc.Uppercase(req.S)
		if err != nil {
			return uppercaseResponse{v, err.Error()}, nil
		}
		return uppercaseResponse{v, ""}, nil
	}
}
