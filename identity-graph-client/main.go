
// Package main implements a client for Greeter service.
package main

import (
	"context"
	"fmt"
	addpb "github.com/go-kit/kit/examples/addsvc/pb"
	"github.com/openzipkin/zipkin-go"
	zipkingrpc "github.com/openzipkin/zipkin-go/middleware/grpc"
	zipkinhttp "github.com/openzipkin/zipkin-go/reporter/http"
	"google.golang.org/grpc"
	"log"
	"os"
	"time"
)

const (
	address     = "localhost:8082"
	defaultName = "world"
)

func main() {
	var zipkinTracer *zipkin.Tracer
	{
		var zipkinURL = "http://localhost:9411/api/v2/spans"
		var (
			err         error
			hostPort    = "" // if host:port is unknown we can keep this empty
			serviceName = "addsvc-client"
			reporter    = zipkinhttp.NewReporter(zipkinURL)
		)
		defer reporter.Close()
		zEP, _ := zipkin.NewEndpoint(serviceName, hostPort)
		zipkinTracer, err = zipkin.NewTracer(reporter, zipkin.WithLocalEndpoint(zEP))
		if err != nil {
			fmt.Fprintf(os.Stderr, "unable to create zipkin tracer: %s\n", err.Error())
			os.Exit(1)
		}

	}
	// Set up a connection to the server.
	conn, err := grpc.Dial(address,
		grpc.WithStatsHandler(zipkingrpc.NewClientHandler(zipkinTracer)),
		grpc.WithInsecure(),
		grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := addpb.NewAddClient(conn)

	// Contact the server and print out its response.
	//name := defaultName
	//if len(os.Args) > 1 {
	//	name = os.Args[1]
	//}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Concat(ctx, &addpb.ConcatRequest{A: "1", B: "2"})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Greeting: %s", r.GetV())
}
