
###How to run the project ?

     
    first install librdkafka before running DataProcessorService
    
    brew install librdkafka pkg-config
    
    on MAC fix the path for package-config
    
    cat /usr/local/Cellar/librdkafka/1.2.2/lib/pkgconfig
    export PKG_CONFIG_PATH=/usr/lib/pkgconfig:/usr/local/lib/pkgconfig/:/usr/local/Cellar/librdkafka/1.2.2/lib/pkgconfig
    add the above statement to your ~/.bash_profile
    
    PKG_CONFIG_PATH=/usr/lib/pkgconfig:/usr/local/lib/pkgconfig/:/usr/local/Cellar/librdkafka/0.11.6/lib/pkgconfig/
    git clone https://github.com/edenhill/librdkafka.git
    cd librdkafka
    ./configure --prefix /usr
    make
    sudo make install
    
    after running the main, hit the service like this
    curl -XPOST -d'{"s":"hello, world"}' localhost:8080/uppercase 
    curl -XPOST -d'{"s":"hello, world"}' localhost:8080/count

OpenZipkin for tracing
run a docker instance of openzipkin
docker run -d -p 9411:9411 openzipkin/zipkin
docker run imageId

http://localhost:9411/zipkin/dependency?endTs=1574298933104&startTs=1574212533104