package kafka_consumer

import (
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	log "github.com/sirupsen/logrus"
	"time"
)
type KafkaConsumer struct {
	consumer *kafka.Consumer
	MessageChannel chan string
}

func New(servers string, groupId string, offset string) *KafkaConsumer {
	var consumer KafkaConsumer
	consumer.ConnectToKafka(servers, groupId, offset)
	consumer.MessageChannel = make(chan string, 1000)
	return &consumer
}

func (k *KafkaConsumer) ConnectToKafka(servers string, groupId string, offset string) {
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": servers,
		"group.id":          groupId,
		"auto.offset.reset": offset, //"earliest"
	})
	if err != nil {
		panic(err)
	}
	k.consumer = c;
}

func (k *KafkaConsumer) ConsumeTopic(topic string) {
	k.consumer.SubscribeTopics([]string{topic}, nil)
	go func() {
		for {
			msg, err := k.consumer.ReadMessage(10* time.Second)
			if err == nil {
				msgRead := string(msg.Value)
				//fmt.Printf("Message on %s: %s\n", msg.TopicPartition, msgRead)
				log.WithFields(log.Fields{
					"msg": msg,
				}).Info("read message from kafka", msgRead)
				k.MessageChannel <- msgRead
			} else {
				// The client will automatically try to recover from all errors.
				fmt.Printf("Consumer error: %v (%v)\n", err, msg)
			}
		}
	}()

	//
	//	c.Close()
	//}
}