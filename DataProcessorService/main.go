package main

import (
	"awesomeProject/DataProcessorService/kafka_consumer"
	"awesomeProject/example/employee"
	"fmt"
	"github.com/Workiva/go-datastructures/futures"
	_ "github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	_ "github.com/spf13/viper"
	viper "github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"os"
	"sync"
	"testing"
	"time"
)

func TestWaitOnGetResult(t *testing.T) {
	completer := make(chan interface{})
	f := futures.New(completer, time.Duration(30 * time.Minute))
	var result interface{}
	var err error
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		result, err = f.GetResult()
		wg.Done()
	}()

	completer <- `test`
	wg.Wait()

	assert.Nil(t, err)
	assert.Equal(t, `test`, result)

	// ensure we don't get paused on the next iteration.
	result, err = f.GetResult()

	assert.Equal(t, `test`, result)
	assert.Nil(t, err)
}

func readConfig() {
	viper.SetConfigName("config.yaml") // name of config file (without extension)
	viper.AddConfigPath("/etc/DataProcessorService/config/")   // path to look for the config file in
	viper.AddConfigPath("$HOME/go/src/awesomeProject/DataProcessorService/config")  // call multiple times to add many search paths
	viper.AddConfigPath(".")               // optionally look for config in the working directory
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})
	var consumer = kafka_consumer.KafkaConsumer{};
	consumer.ConnectToKafka(viper.GetString("kafka.server"),
				   viper.GetString("kafka.groupId"),
				   viper.GetString("kafka.offset"));

	viper.Get("name");

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.InfoLevel)
}

// Transports expose the service to the network. In this first example we utilize JSON over HTTP.
func main() {
	readConfig()
	e1 := employee.New("Sam", "Adolf", 30, 20)
	e1.LeavesRemaining()

	log.WithFields(log.Fields{
		"animal": "walrus",
		"size":   10,
	}).Info("A group of walrus emerges from the ocean")

	//svc := stringService{}
	//
	//uppercaseHandler := httptransport.NewServer(
	//	makeUppercaseEndpoint(svc),
	//	decodeUppercaseRequest,
	//	encodeResponse,
	//)
	//
	//countHandler := httptransport.NewServer(
	//	makeCountEndpoint(svc),
	//	decodeCountRequest,
	//	encodeResponse,
	//)
	//
	//http.Handle("/uppercase", uppercaseHandler)
	//http.Handle("/count", countHandler)
	//log.Fatal(http.ListenAndServe(":8080", nil))
}

